## piforth project Makefile

# name of kernel image
KERNELIMG = kernel.img
# name of kernel ELF
KERNELELF = kernel.elf
# name of kernel linker script
KERNELLD = kernel.ld

# collect names of object files from names of assembler source files
OBJFILES := $(CSRC:.c=.o)
OBJFILES += $(SSRC:.s=.o)

# configuration
TRIPLET = arm-none-eabi
PLATFORMOPT = -mcpu=arm1176jzf-s

# configure assembler
AS = $(TRIPLET)-as
ASOPT = $(PLATFORMOPT)

RUSTC = rustc
RUSTCOPT = --target arm-unknown-linux-gnueabihf -O 

# configure linker
LD = $(TRIPLET)-ld
LDOPT = --error-unresolved-symbols

# configure objcopy
OBJCOPY = $(TRIPLET)-objcopy
OBJCOPYOPT =

# lists of files
ASSRC = $(wildcard src/*.s) \
	$(wildcard src/bootstrap/*.s)
ASOBJ = $(ASSRC:.s=.o)
RSSRC = $(wildcard src/*.rs) \
	$(wildcard src/bootstrap/*.rs) \
	$(wildcard src/kernel/*.rs) \
	$(wildcard src/io/*.rs)
RSOBJ = $(RSSRC:.rs=.o)
OBJS = $(ASOBJ) $(RSOBJ)

# compile assembler source files
%.o : %.s
	@echo -n "Assembling '$<'... "
	@$(AS) -o $@ $<
	@echo "done."

# compile Rust source files
%.o : %.rs
	@echo -n "Compiling '$<'... "
	@$(RUSTC) $(RUSTCOPT) --emit=obj -L libs -o $@ $<
	@echo "done."

#############################
# targets 
#############################

# default is to build the kernel image
.PHONY: all
all: kernel.img

# produce final kernel image
kernel.img : $(OBJS)
	@echo -n "Linking kernel... "
	@$(LD) $(LDOPT) --script $(KERNELLD) -o $(KERNELELF) $(OBJS)
	@echo -ne "done.\nCreating kernel image... "
	@$(OBJCOPY) $(OBJCOPYOPT) -O binary $(KERNELELF) $(KERNELIMG)
	@echo "done."

# delete intermediate and output files
.PHONY: clean
clean:
	@echo -n "Deleting output files... "
	@rm -f $(KERNELIMG)
	@echo -ne "done.\nDeleting intermediate files... "
	@rm -f $(KERNELELF)
	@echo -ne "done.\nDeleting object files... "
	@rm -f $(OBJS)
	@echo "done."
